local Character = require 'common.class' ()
local Skill = require 'model.skill'

function Character:_init(spec)
  self.spec = spec
  self.appearance = spec.appearance
  self.hp = spec.max_hp
  self.str = spec.str
  self.def = spec.def
  self.spd = spec.spd
  self.death_exp = spec.death_exp
  self.exp = spec.exp
  self.levelup_exp = spec.levelup_exp
  self.levelup_atr = spec.levelup_atr
  self.items = {}
  self.max_items = 2

  if spec.level then
    self.level = math.max(1, spec.level)
    self:set_exp(self:get_exp() + (self.level-1)*self.levelup_exp)
  end

  local fight_spec = require('database.skills.' .. spec.fight)
  self.fight = Skill(fight_spec)

  local skill_spec = require('database.skills.' .. spec.skill)
  self.skill = Skill(skill_spec)
end

function Character:get_name()
  return self.spec.name
end

function Character:get_danger_threshold()
  return self.spec.danger_threshold
end

function Character:get_ai()
  return self.spec.ai
end

function Character:set_exp(earned_exp)
  self.exp = earned_exp
end

function Character:get_exp()
  return self.exp
end

function Character:levelup()
  self.level = self.level + 1
end

function Character:lup_exp()
  return self.levelup_exp
end

function Character:lup_atr()
  return self.levelup_atr
end

function Character:add_item(item_add)
  local current_items = 0
  for _,item in pairs(self.items) do
    if item.type == item_add.type then
      return 'type'
    end
    current_items = current_items + 1
  end
  if current_items >= self.max_items then
    return 'full'
  else
    self.items[current_items] = item_add
    return 'correct'
  end
end

function Character:remove_item(item_removed)
  for i,item in pairs(self.items) do
    if item == item_removed then
      self.items[i] = nil
      return i
    end
  end
  return false
end

function Character:get_appearance()
  return self.appearance
end

function Character:reset_appearance()
  self.appearance = self.spec.appearance
end

function Character:get_hp()
  local temp = self.spec.max_hp
  if self.level then
    temp = temp + (self.level-1)*self.levelup_atr.hp
  end
    return self.hp, temp
end

function Character:set_hp(value)
  if self.level then
    self.hp = math.min(math.max(value, 0), self.spec.max_hp + (self.level-1)*self.levelup_atr.hp)
  else
    self.hp = math.min(math.max(value, 0), self.spec.max_hp)
  end
end

function Character:reset_hp()
  if self.level then
    self.hp = self.spec.max_hp + (self.level-1)*self.levelup_atr.hp
  else
    self.hp = self.spec.max_hp
  end
end

function Character:get_atr(atr)
  if atr == 'str' then
    return self:get_str()

  elseif atr == 'def' then
    return self:get_def()

  elseif atr == 'spd' then
    return self:get_spd()

  else
    return 0

  end
end

function Character:get_str()
  return self.str, self.spec.str
end

function Character:get_def()
  return self.def, self.spec.def
end

function Character:get_spd()
  return self.spd, self.spec.spd
end

function Character:set_str(value)
  self.str = math.max(value, 0)
end

function Character:set_def(value)
  self.def = math.max(value, 0)
end

function Character:set_spd(value)
  self.spd = math.max(value, 0)
end

function Character:get_fight()
  return self.fight
end

function Character:get_skill()
  return self.skill
end

function Character:receive_damage(quantity)
  self.hp = self.hp - quantity
  self.hp = math.min(self.spec.max_hp, math.max(self.hp, 0))

  if self:is_dead() then
    self.appearance = 'dead'
  else
    self:reset_appearance()
  end

end

function Character:is_dead()
  return self.hp == 0
end

return Character

