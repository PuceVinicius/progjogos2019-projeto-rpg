
local Skill = require 'common.class' ()

function Skill:_init(spec)
  self.spec = spec
  self.uses = spec.max_uses
end

function Skill:get_name()
  return self.spec.name
end

function Skill:get_message()
  return self.spec.message
end

function Skill:get_uses()
  return self.uses, self.spec.max_uses
end

function Skill:reduce_uses()
  self.uses = self.uses - 1
end

function Skill:renew_uses()
  self.uses = self.spec.max_uses
end

function Skill:get_target()
  return self.spec.target
end

function Skill:get_power()
  return self.spec.power
end

function Skill:get_atk_atr()
  return self.spec.atk_atr
end

function Skill:get_def_atr()
  return self.spec.def_atr
end

function Skill:get_spec(spec)
	return self.spec[spec]
end

return Skill

