
return {
  title = 'Slime Obliteration',
  party = { 'big_warrior' },
  encounters = {
    { 'blue_slime', 'green_slime' },
    { 'green_slime', 'blue_slime', 'green_slime' },
    { 'mega_slime', 'mega_slime', 'blue_slime' },
  },
  description = {
    'The Exterminator is going to destroy the slime spawner!',
    'Some slimes are trying to STOP the Exterminator',
    'The EPIC battle between the Exterminator and the slime bosses!'
  },
  items = {'Potion', 'Str Tonic', 'Def Tonic', 'Spd Tonic'},
  item_values = {
    potion = 10,
    str_tonic = 10,
    def_tonic = 10,
    spd_tonic = 10
  },
  equips = {'Arcane Ring', 'Demon Dagger', 'Power Ring',
   'Enormous Sword', 'Magical Boots', 'Steel Greaves'}
}

