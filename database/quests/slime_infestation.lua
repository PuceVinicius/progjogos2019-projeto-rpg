
return {
  title = 'Slime Infestation',
  party = { 'warrior', 'archer', 'priest' },
  encounters = {
    { 'green_slime' },
    { 'green_slime', 'green_slime', 'green_slime' },
    { 'blue_slime', 'green_slime' },
  },
  description = {
    'Three adventurers crossed a lake and found a slime!',
    'The slime triplicates itself after death!',
    'Some blobs get together and form a giant blue slime!'
  },
  items = {'Potion', 'Str Tonic', 'Def Tonic', 'Spd Tonic'},
  item_values = {
    potion = 10,
    str_tonic = 1,
    def_tonic = 1,
    spd_tonic = 1
  },
  equips = {'Arcane Ring', 'Demon Dagger', 'Power Ring',
  'Enormous Sword', 'Magical Boots', 'Steel Greaves',
  'Amazon Armor', 'Dragon Scale Mail'}
}

