
return {
  name = "Miracle",
  message = "%s used a miracle on %s!",
  max_uses = 5,
  target = "allies",
  revives = true,
  power = -2,
  atk_atr = "str",
  def_atr = nil,
}

