
return {
  name = "Fight",
  message = "%s attacked %s",
  max_uses = 1000,
  target = "enemies",
  power = 1,
  atk_atr = "str",
  def_atr = "def",
}

