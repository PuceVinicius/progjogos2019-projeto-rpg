
return {
  name = "Backstab",
  message = "%s backstabbed %s",
  max_uses = 2,
  target = "enemies",
  power = 5,
  atk_atr = "str",
  def_atr = "def",
}

