
return {
  name = "Heal",
  message = "%s healed %s!",
  max_uses = 5,
  target = "allies",
  power = -1,
  atk_atr = "str",
  def_atr = nil,
}

