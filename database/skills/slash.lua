
return {
  name = "Slash",
  message = "%s slashed %s!",
  max_uses = 10,
  target = "enemies",
  power = 2,
  atk_atr = "str",
  def_atr = "def",
  hit = 50,
  crit = 50,
  crit_message = "%s hit a critical slash on %s!",
}

