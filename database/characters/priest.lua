
return {
  name = "Friendly Priest",
  appearance = 'priest',
  max_hp = 8,
  str = 2,
  def = 5,
  spd = 20,
  fight = 'heal',
  skill = 'miracle',
  level = 1,
  levelup_exp = 9,
  levelup_atr = {hp = 1, str = 1, def = 3, spd = 5},
  exp = 0
}

