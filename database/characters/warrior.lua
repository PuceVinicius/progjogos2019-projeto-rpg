
return {
  name = "Veteran Warrior",
  appearance = 'knight',
  max_hp = 20,
  str = 10,
  def = 15,
  spd = 0,
  fight = 'fight',
  skill = 'slash',
  level = 1,
  levelup_exp = 15,
  levelup_atr = {hp = 4, str = 4, def = 3, spd = 1},
  exp = 0
}

