
return {
  name = "Recluse Archer",
  appearance = 'archer',
  max_hp = 12,
  str = 20,
  def = 0,
  spd = 15,
  fight = 'fight',
  skill = 'backstab',
  level = 2,
  levelup_exp = 12,
  levelup_atr = {hp = 2, str = 5, def = 1, spd = 4},
  exp = 0
}

