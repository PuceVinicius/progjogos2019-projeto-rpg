
return {
  name = "Blue Slime",
  appearance = 'blue_slime',
  max_hp = 16,
  str = 10,
  def = 15,
  spd = 10,
  fight = 'fight',
  skill = 'heal',
  ai = 'crush_the_weak',
  danger_threshold = 0.3,
  death_exp = 8
}

