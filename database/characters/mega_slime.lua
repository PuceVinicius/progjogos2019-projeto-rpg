
return {
  name = "Mega Slime",
  appearance = 'mega_slime',
  max_hp = 200,
  str = 21,
  def = 7,
  spd = -25,
  fight = 'fight',
  skill = 'heal',
  ai = 'crush_the_weak',
  danger_threshold = 0.3,
  death_exp = 800
}

