
return {
  name = "Exterminator",
  appearance = 'prestige_knight',
  max_hp = 23,
  str = 13,
  def = 13,
  spd = 15,
  fight = 'fight',
  skill = 'slash',
  level = 1,
  levelup_exp = 7,
  levelup_atr = {hp = 3, str = 4, def = 1, spd = 0},
  exp = 0
}

