
return {
  name = "Green Slime",
  appearance = 'slime',
  max_hp = 6,
  str = 5,
  def = 5,
  spd = 5,
  fight = 'fight',
  skill = 'heal',
  ai = 'random',
  danger_threshold = 0.5,
  death_exp = 3
}

