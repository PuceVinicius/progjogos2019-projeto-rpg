
return {
  black = { .1, .1, .1 },
  red = { 1, .4, .4 },
  green = { .4, 1, .4 },
  blue = { .4, .4, 1 },
  white = { .7, .7, .9 },
  gray = { .5, .5, .5 }
}

