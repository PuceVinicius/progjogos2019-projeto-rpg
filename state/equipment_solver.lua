local State = require 'state'

local equipment_solver = require 'common.class' (State)

function equipment_solver:_init(stack)
  self:super(stack)
  self.character = nil
  self.item = nil
  self.options = self:_load_equips()
end

function equipment_solver:_load_equips()
  local options = {}
  local equip_names = love.filesystem.getDirectoryItems('database/equips')
  self.equips = {}
  for i, name in ipairs(equip_names) do
    name = name:sub(1, -5)
    local equip = require('database.equips.' .. name)
    options[i] = equip.name
    self.equips[i] = equip
  end
  return options
end

function equipment_solver:enter(params)

  self.character = params.character

  for _, item in ipairs(self.equips) do
    if item.name == params.item then
      self.item = item
    end
  end

  params.validation = false
  params.type = self.item.type

  local result = self.character:add_item(self.item)

  if result == 'correct' then

    params.validation = result

    if self.item.str then
      local str,_ = self.character:get_str()
      self.character:set_str(str + self.item.str)
    end

    if self.item.def then
      local def,_ = self.character:get_def()
      self.character:set_def(def + self.item.def)
    end

    if self.item.spd then
      local spd,_ = self.character:get_spd()
      self.character:set_spd(spd + self.item.spd)
    end
  else
    params.validation = result
  end

  local output = {
    action = 'Equipment',
    equip = {name = self.item.name, type = params.type, validation = params.validation},
    target = self.character
  }

  self:pop( output )

end

return equipment_solver

