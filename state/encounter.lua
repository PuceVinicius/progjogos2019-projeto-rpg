
local Vec = require 'common.vec'
local MessageBox = require 'view.message_box'
local SpriteAtlas = require 'view.sprite_atlas'
local BattleField = require 'view.battlefield'
local ListMenu = require 'view.list_menu'
local State = require 'state'

local EncounterState = require 'common.class' (State)

local CHARACTER_GAP = 96

local MESSAGES = {
  Item = '%s used %s on %s',
  Equipment = '%s equipped %s',
  Used = '%s already has a %s',
  Full = '%s is already full of items'
}

local function turn_order(char1, char2)
  local spd1, _ = char1:get_spd()
  local spd2, _ = char2:get_spd()
  return spd1 > spd2
end

function EncounterState:_init(stack)
  self:super(stack)
  self.turns = nil
  self.allies = nil
  self.enemies = nil
  self.next_turn = nil
end

function EncounterState:enter(params)
  local atlas = SpriteAtlas()
  local battlefield = BattleField()
  local bfbox = battlefield.bounds
  local message = MessageBox(Vec(bfbox.left, bfbox.bottom + 60))
  local n = 0
  local party_origin = battlefield:east_team_origin()
  self.turns = {}
  self.allies = {}
  self.enemies = {}
  self.items = params.items
  self.item_values = params.item_values
  self.equips = params.equips
  self.next_turn = 1
  for i, character in ipairs(params.party) do
    local pos = party_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    self.turns[i] = character
    self.allies[i] = character
    atlas:add(character, pos, character:get_appearance())
    n = n + 1
  end
  local encounter_origin = battlefield:west_team_origin()
  for i, character in ipairs(params.encounter) do
    local pos = encounter_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    self.turns[n + i] = character
    self.enemies[i] = character
    atlas:add(character, pos, character:get_appearance())
  end
  self:view():add('atlas', atlas)
  self:view():add('battlefield', battlefield)
  self:view():add('message', message)
  table.sort(self.turns, turn_order)
  message:set(params.description)
end

function EncounterState:reset_atlas()
  self:view():get('atlas'):clear()

  local atlas = SpriteAtlas()
  local battlefield = BattleField()
  local party_origin = battlefield:east_team_origin()
  local encounter_origin = battlefield:west_team_origin()

  for i, character in ipairs(self.allies) do
    local pos = party_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    atlas:add(character, pos, character:get_appearance())
  end

  for i, character in ipairs(self.enemies) do
    local pos = encounter_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    atlas:add(character, pos, character:get_appearance())
  end

  self:view():add('atlas', atlas)
end

function EncounterState:leave()
  self:view():get('atlas'):clear()
  self:view():remove('atlas')
  self:view():remove('battlefield')
  self:view():remove('message')
end

function EncounterState:check_win_condition()
  for _, i in ipairs(self.enemies) do
    if not i:is_dead() then return false end
  end
  return true
end

function EncounterState:check_loss_condition()
  for _, i in ipairs(self.allies) do
    if not i:is_dead() then return false end
  end
  return true
end

function EncounterState:is_enemy(character)
  for _, enemy in ipairs(self.enemies) do
    if character == enemy then return true end
  end
  return false
end

function EncounterState:exp_allies()
  local total_exp = 0
  for _, enemy in ipairs(self.enemies) do
    total_exp = total_exp + enemy.death_exp
  end
  for _, allie in ipairs(self.allies) do
    self:earn_exp(allie, total_exp)
  end
end

function EncounterState:earn_exp(char, earned_exp)
  if char:get_exp() + earned_exp >= char:lup_exp() then
    earned_exp = earned_exp + char:get_exp() - char:lup_exp()
    char:set_exp(0)
    self.levelup(char)
    self:earn_exp(char, earned_exp)
  else
    char:set_exp(char:get_exp() + earned_exp)
  end
end

function EncounterState.levelup(char)
  char:set_hp(char:get_hp() + char:lup_atr().hp)
  char:set_str(char:get_str() + char:lup_atr().str)
  char:set_def(char:get_def() + char:lup_atr().def)
  char:set_spd(char:get_spd() + char:lup_atr().spd)
end

function EncounterState:update(_)
  self:reset_atlas()
  if self:check_win_condition() then
    self:exp_allies()
    return self:pop({})
  end
  if self:check_loss_condition() then return self:pop({loss = true}) end
  local current_character = self.turns[self.next_turn]
  self.next_turn = self.next_turn % #self.turns + 1
  while current_character:is_dead() do
    current_character = self.turns[self.next_turn]
    self.next_turn = self.next_turn % #self.turns + 1
  end
  local params = {
    current_character = current_character,
    allies = self.allies,
    enemies = self.enemies,
    items = self.items,
    equips = self.equips
  }
  if self:is_enemy(current_character) then
    return self:push('enemy_turn', params)
  else
    return self:push('player_turn', params)
  end
end

function EncounterState:resume(params)
  if params.from == 'player_turn' then
    if params.action ~= 'Run' then
      local message

      if params.action == 'Fight' then
        local skill_params = { attacker = params.character, defender = params.target, type = 'fight' }
        self:push('resolve_skill', skill_params)

      elseif params.action == 'Skill' then
        local skill_params = { attacker = params.character, defender = params.target, type = 'skill' }
        self:push('resolve_skill', skill_params)


      elseif params.action == 'Equipment' then
        if params.equip.validation == 'correct' then
          message = MESSAGES.Equipment:format(params.character:get_name(), params.equip.name)
        elseif params.equip.validation == 'type' then
          message = MESSAGES.Used:format(params.character:get_name(), params.equip.type)
        else
          message = MESSAGES.Full:format(params.character:get_name())
        end
        self:view():get('message'):set(message)

      else
        self.menu = ListMenu(self.items)
        message = MESSAGES.Item:format(params.character:get_name(), params.action, params.target:get_name())
        self:view():get('message'):set(message)
        if params.action == 'Potion' then
          local hp, _ = params.target:get_hp()
          params.target:set_hp(hp + self.item_values.potion)
        elseif params.action == 'Str Tonic' then
          local str, _ = params.target:get_str()
          params.target:set_str(str + self.item_values.str_tonic)
        elseif params.action == 'Def Tonic' then
          local def, _ = params.target:get_def()
          params.target:set_def(def + self.item_values.def_tonic)
        elseif params.action == 'Spd Tonic' then
          local spd, _ = params.target:get_spd()
          params.target:set_spd(spd + self.item_values.spd_tonic)
        end

      end

    else
      return self:pop({loss = true})
    end

  end

end

return EncounterState


