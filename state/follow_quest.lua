
local Character = require 'model.character'
local State = require 'state'

local FollowQuestState = require 'common.class' (State)

function FollowQuestState:_init(stack)
  self:super(stack)
  self.party = nil
  self.encounters = nil
  self.items = nil
  self.item_values = nil
  self.equips = nil
  self.next_encounter = nil
end

function FollowQuestState:enter(params)
  local quest = params.quest
  self.encounters = quest.encounters
  self.description = quest.description
  self.items = quest.items
  self.item_values = quest.item_values
  self.equips = quest.equips
  self.next_encounter = 1
  self.party = {}
  for i, character_name in ipairs(quest.party) do
    local character_spec = require('database.characters.' .. character_name)
    self.party[i] = Character(character_spec)
  end
end

function FollowQuestState:update(_)
  if self.next_encounter <= #self.encounters then
    local encounter = {}
    local encounter_specnames = self.encounters[self.next_encounter]
    self.next_encounter = self.next_encounter + 1
    for i, character_name in ipairs(encounter_specnames) do
      local character_spec = require('database.characters.' .. character_name)
      encounter[i] = Character(character_spec)
    end
    local params = {
      party = self.party,
      encounter = encounter,
      description = self.description[self.next_encounter-1],
      items = self.items,
      item_values = self.item_values,
      equips = self.equips
    }
    return self:push('encounter', params)
  else
    return self:pop()
  end
end

function FollowQuestState:resume(params)
  --If party is party is dead, game over
  if params.loss then
    return self:pop()
  end

  --Else, renew_party
  self:renew_party()

end

function FollowQuestState:renew_party()
  for _, char in ipairs(self.party) do
    if not char:is_dead() then
      char:reset_hp()
      char:get_skill():renew_uses()
      --reset skill use etc
    end
  end
end

return FollowQuestState


