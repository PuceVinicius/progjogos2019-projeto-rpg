local State = require 'state'

local resolve_skill = require 'common.class' (State)

function resolve_skill:_init(stack)
  self:super(stack)
  self.attacker = nil
  self.defender = nil
  self.skill = nil
  self.message = nil
end

function resolve_skill:enter(params)
  self.attacker = params.attacker
  self.defender = params.defender

  local pop_params = { from = 'resolve_skill' }
  local invalid = false

  if params.type == 'fight' then
    self.skill = self.attacker:get_fight()
  else
    self.skill = self.attacker:get_skill()
    local uses, _ = self.skill:get_uses()

    if uses > 0 then
      self.skill:reduce_uses()
    else
      self.message = "This skill is all used up!"
      invalid = true
    end

  end

  if self.defender:is_dead() and not self.skill:get_spec('revives') then
    self.message = "Target is already dead!"
    invalid = true
  end

  if self.skill:get_spec('hit') and math.random(1,100) > self.skill:get_spec('hit') then
    self.message = "You missed!"
    invalid = true
  end

  if not invalid then
    self.message = self.skill:get_message():format(self.attacker:get_name(), self.defender:get_name())
    self:resolve()
  end

  self:view():get('message'):set(self.message)

  self:pop(pop_params)

end

function resolve_skill:resolve()
  local atk_atr = self.attacker:get_atr(self.skill:get_atk_atr())
  local def_atr = self.defender:get_atr(self.skill:get_def_atr())

  local damage

  if self.skill:get_spec('crit') and math.random(1,100) > self.skill:get_spec('crit') then
    self.message = self.skill:get_spec('crit_message'):format(self.attacker:get_name(), self.defender:get_name())
    atk_atr = atk_atr * 3
  end

  if def_atr >= atk_atr then
    damage = 0
  else
    damage = (atk_atr * self.skill:get_power()) - def_atr
  end

  self.defender:receive_damage(damage)

end

return resolve_skill

