
local Vec = require 'common.vec'
local CharacterStats = require 'view.character_stats'
local TurnCursor = require 'view.turn_cursor'
local ListMenu = require 'view.list_menu'
local State = require 'state'

local PlayerTurnState = require 'common.class' (State)

local list_view = 'turn'

local TURN_OPTIONS = { 'Fight', 'Skill', 'Item', 'Equipment', 'Run' }
local ITEM_OPTIONS = {}
local EQUIP_OPTIONS = {}

function PlayerTurnState:_init(stack)
  self:super(stack)
  self.character = nil
  self.targets = nil
  self.menu = ListMenu(TURN_OPTIONS)
end

function PlayerTurnState:enter(params)
  self.character = params.current_character
  self.allies = params.allies
  self.enemies = params.enemies
  ITEM_OPTIONS = params.items
  EQUIP_OPTIONS = params.equips
  self:_show_menu()
  self:_show_cursor()
  self:_show_stats()
end

function PlayerTurnState:_show_menu()
  local bfbox = self:view():get('battlefield').bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 32, (bfbox.top + bfbox.bottom) / 2 + 16)
  self:view():add('turn_menu', self.menu)
end

function PlayerTurnState:_show_cursor()
  local atlas = self:view():get('atlas')
  local sprite_instance = atlas:get(self.character)
  local cursor = TurnCursor(sprite_instance)
  self:view():add('turn_cursor', cursor)
end

function PlayerTurnState:_show_stats()
  local bfbox = self:view():get('battlefield').bounds
  local position = Vec(bfbox.right + 16, bfbox.top)
  local char_stats = CharacterStats(position, self.character)
  self:view():add('char_stats', char_stats)
end

function PlayerTurnState:leave()
  self:view():remove('turn_menu')
  self:view():remove('turn_cursor')
  self:view():remove('char_stats')
end

function PlayerTurnState:on_keypressed(key)
  if key == 'down' then
    self.menu:next()
  elseif key == 'up' then
    self.menu:previous()
  elseif key == 'return' then
    local option
    if list_view == 'turn' then
      option = TURN_OPTIONS[self.menu:current_option()]
    elseif list_view == 'items' then
      option = ITEM_OPTIONS[self.menu:current_option()]
      local params = self:_goto_target_selection(option)
      self.menu = ListMenu(TURN_OPTIONS)
      list_view = 'turn'
      return self:push('target_select', params)
    elseif list_view == 'equips' then
      option = EQUIP_OPTIONS[self.menu:current_option()]
      local params = {
        character = self.character,
        item = option
      }
      self.menu = ListMenu(TURN_OPTIONS)
      list_view = 'turn'
      self:_show_menu()
      return self:push('equipment_solver', params)
    end
    if option == 'Item' then
      self.menu = ListMenu(ITEM_OPTIONS)
      list_view = 'items'
      local characters = {}
      for _, ally in pairs(self.allies) do
        table.insert(characters, ally)
      end
      for _, enemy in pairs(self.enemies) do
        table.insert(characters, enemy)
      end
      local params = {
        current_character = self.character,
        allies = characters,
        enemies = characters,
        items = ITEM_OPTIONS
      }
      return self:push('player_turn', params)
    end
    if option == 'Equipment' then
      self.menu = ListMenu(EQUIP_OPTIONS)
      list_view = 'equips'
      local params = {
        current_character = self.character,
        allies = self.allies,
        enemies = self.enemies,
        equips = EQUIP_OPTIONS
      }
      return self:push('player_turn', params)
    end
    if option == 'Fight' or option == 'Skill' then
      local params = self:_goto_target_selection(option)
      return self:push('target_select', params)
    else
      return self:pop({ from = 'player_turn', action = option, character = self.character })
    end
  end
end

function PlayerTurnState:_goto_target_selection(option)
  self:view():remove('turn_cursor')
  local params = { action = option, attacker = self.character, allies = self.allies, enemies = self.enemies }
  return params
end

function PlayerTurnState:resume(params)
  if params.target ~= nil then
    local pop_prm = {
      from = 'player_turn',
      action = params.action,
      character = self.character,
      target = params.target,
      equip = params.equip
    }
    return self:pop(pop_prm)
  else
    self:_show_cursor()
  end
end

return PlayerTurnState

