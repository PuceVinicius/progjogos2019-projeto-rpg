
local Vec = require 'common.vec'
local CharacterStats = require 'view.character_stats'
local TurnCursor = require 'view.turn_cursor'
local State = require 'state'

local target_select = require 'common.class' (State)

function target_select:_init(stack)
  self:super(stack)
  self.attacker = nil
  self.available_targets = nil
  self.target = nil
  self.target_num = nil
  self.action = nil
end

function target_select:enter(params)
  self.attacker = params.attacker
  self.action = params.action
  self.target_num = 1

  local skill

  if self.action == 'Fight' then
    skill = self.attacker:get_fight()
  else
    skill = self.attacker:get_skill()
  end

  if skill:get_target() == 'allies' then
      self.available_targets = params.allies
  else
      self.available_targets = params.enemies
  end

  self.target = self.available_targets[self.target_num]

  self:_show_cursor()
  self:_show_stats()
  self:view():get('message'):set('Select your target.')
end

function target_select:_show_cursor()
  local atlas = self:view():get('atlas')
  local sprite_instance = atlas:get(self.target)
  local cursor = TurnCursor(sprite_instance)
  self:view():add('turn_cursor', cursor)
end

function target_select:_show_stats()
  local bfbox = self:view():get('battlefield').bounds
  local position = Vec(bfbox.right + 16, bfbox.top)
  local char_stats = CharacterStats(position, self.target)
  self:view():add('char_stats', char_stats)
end

function target_select:leave()
  self:view():remove('turn_cursor')
  self:view():remove('char_stats')
end

function target_select:on_keypressed(key)
  if key == 'down' or key == 'right' then
    self.target_num = self.target_num % #self.available_targets + 1
    self:_update_cursor()
  elseif key == 'up' or key == 'left' then
    self.target_num = (self.target_num - 2) % #self.available_targets + 1
    self:_update_cursor()
  elseif key == 'return' then
    return self:pop({ action = self.action, target = self.target })
  end
end

function target_select:_update_cursor()
  self:view():remove('turn_cursor')
  self:view():remove('char_stats')
  self.target = self.available_targets[self.target_num]
  self:_show_cursor()
  self:_show_stats()
end

return target_select

