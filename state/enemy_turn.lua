
local State = require 'state'

local EnemyTurnState = require 'common.class' (State)

function EnemyTurnState:_init(stack)
  self:super(stack)
  self.character = nil
end

function EnemyTurnState:enter(params)
  self.character = params.current_character
  self.allies = params.allies
  self.enemies = params.enemies
  self:process()
end

function EnemyTurnState:process()
  local hp, max_hp = self.character:get_hp()
  local skill_params
  if hp < self.character:get_danger_threshold() * max_hp then
    skill_params = { attacker = self.character, defender = self.character, type = 'heal' }
    self:push('resolve_skill', skill_params)
    return self:pop({ from = 'enemy_turn', action = 'Skill', character = self.character })
  end
  if self.character:get_ai() == 'random' then
    local defender
    for _, ally in pairs(self.allies) do
      if not ally:is_dead() then
        defender = ally
      end
    end
    skill_params = { attacker = self.character, defender = defender, type = 'fight' }
  elseif self.character:get_ai() == 'crush_the_weak' then
    local defender = self.allies[1]
    for _, ally in pairs(self.allies) do
      local ally_hp, _ = ally:get_hp()
      local defender_hp, _ = defender:get_hp()
      if ally_hp < defender_hp and not ally:is_dead() then
        defender = ally
      end
    end
    skill_params = { attacker = self.character, defender = defender, type = 'fight' }
  end
  self:push('resolve_skill', skill_params)
  return self:pop({ from = 'enemy_turn', action = 'Fight', character = self.character })
end

return EnemyTurnState

